function varargout = gui002imshowgui(varargin)
% GUI002IMSHOWGUI MATLAB code for gui002imshowgui.fig
%      GUI002IMSHOWGUI, by itself, creates a new GUI002IMSHOWGUI or raises the existing
%      singleton*.
%
%      H = GUI002IMSHOWGUI returns the handle to a new GUI002IMSHOWGUI or the handle to
%      the existing singleton*.
%
%      GUI002IMSHOWGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI002IMSHOWGUI.M with the given input arguments.
%
%      GUI002IMSHOWGUI('Property','Value',...) creates a new GUI002IMSHOWGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui002imshowgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui002imshowgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui002imshowgui

% Last Modified by GUIDE v2.5 16-Oct-2014 14:35:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui002imshowgui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui002imshowgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui002imshowgui is made visible.
function gui002imshowgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui002imshowgui (see VARARGIN)

% Choose default command line output for gui002imshowgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui002imshowgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

filename = 'C:\Users\jun-ko\Dropbox\materials\sample.tif';

%%% figure 1
page = 1;
img = imread(filename, page);
axes(handles.axes1);
imshow(img, []);

% --- Outputs from this function are returned to the command line.
function varargout = gui002imshowgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1
