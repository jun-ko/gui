%%% gui001imshow

%%% tiff fileの読み込み
%function main()
close all; clear all; clc

filename = 'C:\Users\jun-ko\Dropbox\materials\sample.tif';
%% usin dialog
% [fname, dpath] = uigetfile('*.tif', 'load tif');
% filename = strcat(dpath, fname)

finfo = imfinfo(filename);
frames = size(finfo, 1) % all page numbers


%%% figure 1
page = 1;
img = imread(filename, page);
figure; imshow(img, []);

%%%% figure 2
page = 2;
img = imread(filename, page);
figure; imshow(img, []);
